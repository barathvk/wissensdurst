import { observer, inject } from 'mobx-react'
import AnnouncementIcon from 'material-ui-icons/Announcement'
import Identicon from 'react-identity-icon'
import TimeAgo from 'react-timeago'
import moment from 'moment'
@inject('store')
@observer
export default class BuzzWatch extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  render() {
    const store = this.props.store
    return (
      <div className={`flex-row flex-center-align buzzed ${store.buzzed.length === 0 ? 'flex-center' : ''}`}>
        {_.keys(store.buzzed).length > 0 &&
          <div className="flex-row flex-center-align">
            {_.keys(store.buzzed).map((b, i) => {
              return (
                <span key={b} className="flex-row flex-center-align">
                  <Identicon hash={b} size={36} className="avatar" />
                  {i === 0 &&
                    <div className="flex-column animated bounceInUp">
                      <b>
                        {b} &nbsp;
                      </b>
                      <span className="light-gray-text small-text">
                        <TimeAgo date={store.buzzed[b].timestamp} />
                      </span>
                    </div>}
                </span>
              )
            })}
          </div>}
        {_.keys(store.buzzed).length === 0 &&
          <div className="flex-row no-buzz">
            <AnnouncementIcon />
            No one has buzzed yet
          </div>}
      </div>
    )
  }
}
