import { observer, inject } from 'mobx-react'
import Tabs, { Tab } from 'material-ui/Tabs'
import UserToolbar from './UserToolbar'
import Buzzer from './Buzzer'
import Scores from './Scores'
import AlarmIcon from 'material-ui-icons/Alarm'
import StarsIcon from 'material-ui-icons/Stars'
import Paper from 'material-ui/Paper'
import Ticker from './ScoreTicker'
@inject('store')
@observer
export default class User extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      tab: 0
    }
    this.setTab = (e, tab) => {
      this.setState({ tab })
    }
  }
  render() {
    return (
      <div className="fill flex-column">
        <UserToolbar />
        <Ticker />
        <Paper>
          <Tabs value={this.state.tab} onChange={this.setTab} fullWidth indicatorColor="primary" textColor="primary">
            <Tab icon={<AlarmIcon />} label="Buzzer" />
            <Tab icon={<StarsIcon />} label="Score" />
          </Tabs>
        </Paper>
        {this.state.tab === 0 && <Buzzer />}
        {this.state.tab === 1 && <Scores />}
      </div>
    )
  }
}
