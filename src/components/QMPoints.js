import { observer, inject } from 'mobx-react'
import Snackbar from 'material-ui/Snackbar'
import IconButton from 'material-ui/IconButton'
import CloseIcon from 'material-ui-icons/Close'
import StarIcon from 'material-ui-icons/Star'
import HalfStarIcon from 'material-ui-icons/StarHalf'
import MinusIcon from 'material-ui-icons/RemoveCircle'
import SkipNextIcon from 'material-ui-icons/SkipNext'
@inject('store')
@observer
export default class QMPoints extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
    this.dismiss = () => {
      this.props.store.reset()
    }
    this.plus = () => {
      const firstkey = _.first(_.keys(this.props.store.buzzed))
      const first = { ...this.props.store.buzzed[firstkey], team: firstkey }
      this.props.store.qmstore.plus(first)
    }
    this.plushalf = () => {
      const firstkey = _.first(_.keys(this.props.store.buzzed))
      const first = { ...this.props.store.buzzed[firstkey], team: firstkey }
      this.props.store.qmstore.plushalf(first)
    }
    this.minus = () => {
      const firstkey = _.first(_.keys(this.props.store.buzzed))
      const first = { ...this.props.store.buzzed[firstkey], team: firstkey }
      this.props.store.qmstore.minus(first)
    }
    this.next = async () => {
      const firstkey = _.first(_.keys(this.props.store.buzzed))
      this.props.store.qmstore.next(firstkey)
    }
  }
  render() {
    const store = this.props.store
    return (
      <div>
        {_.keys(store.buzzed).length > 0 &&
          <Snackbar
            anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
            open={_.keys(store.buzzed).length > 0}
            message={_.first(_.keys(store.buzzed))}
            style={{ bottom: '69px' }}
            action={[
              <IconButton key="add-1" color="primary" onClick={this.plus}>
                <StarIcon />
              </IconButton>,
              <IconButton key="add-half" color="primary" onClick={this.plushalf}>
                <HalfStarIcon />
              </IconButton>,
              <IconButton color="contrast" key="next" onClick={this.next}>
                <SkipNextIcon />
              </IconButton>,
              <IconButton color="accent" key="minus" onClick={this.minus}>
                <MinusIcon />
              </IconButton>,
              <IconButton color="accent" key="close" onClick={this.dismiss}>
                <CloseIcon />
              </IconButton>
            ]}
          />}
      </div>
    )
  }
}
