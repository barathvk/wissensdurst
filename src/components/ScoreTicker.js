import { observer, inject } from 'mobx-react'
import ChartIcon from 'material-ui-icons/InsertChart'
import StarsIcon from 'material-ui-icons/Stars'
const getOrdinal = n => {
  const s = ['th', 'st', 'nd', 'rd']
  const v = n % 100
  return n + (s[(v - 20) % 10] || s[v] || s[0])
}
@inject('store')
@observer
export default class ScoreTicker extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  render() {
    const store = this.props.store
    const selfpoints = store.scores ? store.scores.filter(s => s.team === store.account.info.team) : []
    const totalpoints = _.sum(selfpoints.map(s => s.points))
    const grouped = _.groupBy(store.scores, s => s.team)
    const places = _.reverse(_.sortBy(_.keys(grouped), k => _.sum(grouped[k].map(v => v.points))))
    const place = getOrdinal(places.indexOf(store.account.info.team) + 1)
    return (
      <div className="flex-row flex-center-align ticker">
        {store.scores &&
          <div className="fill flex-row">
            <div className="fill flex-row flex-center" style={{ marginRight: '8px' }}>
              <ChartIcon className="icon" />
              {totalpoints} point{totalpoints === 1 ? '' : 's'}
            </div>
            <div className="fill flex-row flex-center sparkline" />
            <div className="fill flex-row flex-center">
              <StarsIcon className="icon" />
              {place} place
            </div>
          </div>}
      </div>
    )
  }
}
