import { observer, inject } from 'mobx-react'
import Card, { CardActions, CardContent } from 'material-ui/Card'
import Button from 'material-ui/Button'
import TextField from 'material-ui/TextField'
import { CircularProgress } from 'material-ui/Progress'
import Snackbar from 'material-ui/Snackbar'
@inject('store')
@observer
export default class Login extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
    this.setUsername = e => {
      this.setState({ username: e.target.value })
    }
    this.setPassword = e => {
      this.setState({ password: e.target.value })
    }
    this.login = () => {
      this.props.store.account.login(this.state.username, this.state.password)
    }
    this.signup = async () => {
      await this.props.store.account.signup(this.state.username, this.state.password)
    }
  }
  render() {
    const store = this.props.store
    return (
      <div className="fill flex-column login">
        {!store.account.loading &&
          <div className="fill flex-column flex-center">
            <div className="fancy logo">Wissensdurst</div>
            <Card className="padding-8 flex-column">
              <CardContent className="flex-column fill">
                <TextField label="Email" value={this.state.username || ''} onChange={this.setUsername} style={{ width: '240px' }} type="email" />
                <TextField label="Password" value={this.state.password || ''} onChange={this.setPassword} style={{ width: '240px' }} type="password" />
              </CardContent>
              <CardActions className="flex-row flex-center-align">
                <Button dense raised color="accent" className="fill" onClick={this.signup}>
                  Register
                </Button>
                <Button dense raised color="primary" className="fill" onClick={this.login}>
                  Login
                </Button>
              </CardActions>
            </Card>
          </div>}
        {store.account.loading &&
          <div className="fill flex-column flex-center">
            <CircularProgress className="white-progress" />
          </div>}
        {store.account.error &&
          <Snackbar
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'center'
            }}
            open={store.account.error !== null}
            onRequestClose={this.handleRequestClose}
            message={store.account.error ? store.account.error.message : ''}
          />}
      </div>
    )
  }
}
