import { observer, inject } from 'mobx-react'
@inject('store')
@observer
export default class Buzzer extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
    this.buzz = () => {
      this.props.store.buzz()
    }
  }
  render() {
    return (
      <div className="fill flex-column flex-center animated slideInLeft">
        <div className="buzzer" onClick={this.buzz} />
      </div>
    )
  }
}
