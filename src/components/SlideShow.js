import { observer, inject } from 'mobx-react'
import Particles from 'react-particles-js'
import particlesConfig from '../js/particlesjs-config.json'
import bespoke from 'bespoke'
import theme from 'bespoke-theme-build-wars'
import progress from 'bespoke-progress'
import scale from 'bespoke-scale'
import bullets from 'bespoke-bullets'
import hash from 'bespoke-hash'
@inject('store')
@observer
export default class SlideShow extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
    this.slide = 0
    this.load = () => {
      if (this.props.store.qmstore.slide > 0) {
        if (this.props.store.qmstore.slide === 1 || !this.deck) {
          this.deck = bespoke.from(this.refs.bespoke, [theme(), progress(), scale(), bullets('li')])
          this.deck.on('activate', e => {
            const video = e.slide.querySelector('video')
            if (video) {
              video.play()
            }
            const audio = e.slide.querySelector('audio')
            if (audio) {
              audio.play()
            }
          })
          this.deck.on('deactivate', e => {
            const video = e.slide.querySelector('video')
            if (video) {
              video.pause()
              video.currentTime = 0
            }
            const audio = e.slide.querySelector('audio')
            if (audio) {
              audio.pause()
              audio.currentTime = 0
            }
          })
        }
        if (this.slide < this.props.store.qmstore.slide) {
          this.deck.next()
        } else {
          this.deck.prev()
        }
        if (!this.slide) {
          this.deck.slide(0)
        }
        this.slide = this.props.store.qmstore.slide
      }
    }
  }
  componentDidUpdate() {
    this.load()
  }
  componentDidMount() {
    this.load()
  }

  render() {
    const store = this.props.store
    return (
      <div className="fill flex-column slides">
        {store.qmstore.slide === 0 && (
          <div className="fill flex-column animated slideInLeft">
            <div className="fill flex-column particles">
              <Particles params={particlesConfig} />
            </div>
            <div className="logo fancy">Wissensdurst</div>
          </div>
        )}
        {store.qmstore.slide > 0 && <div className="fill flex-column" ref="bespoke" dangerouslySetInnerHTML={{ __html: store.qmstore.content }} />}
      </div>
    )
  }
}
