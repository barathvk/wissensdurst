import { observer, inject } from 'mobx-react'
@inject('store')
@observer
export default class Admin extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  render() {
    return (
      <div className="flex-column fill flex-center">
        <h1 className="gray-text">Admin</h1>
      </div>
    )
  }
}
