import { observer, inject } from 'mobx-react'
import AppBar from 'material-ui/AppBar'
import Toolbar from 'material-ui/Toolbar'
import Typography from 'material-ui/Typography'
import PowerIcon from 'material-ui-icons/PowerSettingsNew'
import IconButton from 'material-ui/IconButton'
import LockIcon from 'material-ui-icons/LockOutline'
import AspectRatioIcon from 'material-ui-icons/AspectRatio'
import Identicon from 'react-identity-icon'
import RefreshIcon from 'material-ui-icons/Refresh'
import screenfull from 'screenfull'
@inject('store')
@observer
export default class QMToolbar extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
    this.logout = () => this.props.store.account.logout()
    this.showAdmin = () => (this.props.store.showAdmin = true)
    this.navigate = e => {
      this.props.store.qmstore.navigate(e)
    }
    this.toggleSlideShow = () => {
      this.props.store.slides = !this.props.store.slides
      // if (screenfull.enabled) {
      //   screenfull.request()
      // }
    }
  }
  render() {
    const store = this.props.store
    const roles = _.keys(store.account.info.roles)
    return (
      <AppBar position="static">
        <Toolbar className="flex-row">
          <Typography type="title" color="inherit" className="fill flex-row flex-center-align" style={{ display: 'flex' }}>
            <Identicon hash={store.quiz.title} size={22} className="avatar" />
            {store.quiz.title}
          </Typography>
          <IconButton color="contrast" onClick={this.navigate.bind(this, 'reset')}>
            <RefreshIcon />
          </IconButton>
          <IconButton color="contrast" onClick={this.toggleSlideShow}>
            <AspectRatioIcon />
          </IconButton>
          {roles.indexOf('admin') > -1 &&
            <IconButton color="contrast" onClick={this.showAdmin}>
              <LockIcon />
            </IconButton>}
          <IconButton color="contrast" onClick={this.logout}>
            <PowerIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
    )
  }
}
