import { observer, inject } from 'mobx-react'
import QMToolbar from './QMToolbar'
import QMControls from './QMControls'
import QMPoints from './QMPoints'
@inject('store')
@observer
export default class QM extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  render() {
    return (
      <div className="fill flex-column">
        <QMToolbar />
        <QMControls />
        <div className="first-buzz flex-row flex-center-align">
          <QMPoints />
        </div>
      </div>
    )
  }
}
