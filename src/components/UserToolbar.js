import { observer, inject } from 'mobx-react'
import AppBar from 'material-ui/AppBar'
import Toolbar from 'material-ui/Toolbar'
import Typography from 'material-ui/Typography'
import IconButton from 'material-ui/IconButton'
import PowerIcon from 'material-ui-icons/PowerSettingsNew'
import PoopleIcon from 'material-ui-icons/PeopleOutline'
import LockIcon from 'material-ui-icons/LockOutline'
import Identicon from 'react-identity-icon'
@inject('store')
@observer
export default class UserToolbar extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
    this.logout = () => this.props.store.account.logout()
    this.clearTeam = () => (this.props.store.account.info.team = null)
    this.showAdmin = () => (this.props.store.showAdmin = true)
  }
  render() {
    const store = this.props.store
    const roles = _.keys(store.account.info.roles)
    return (
      <AppBar position="static">
        <Toolbar className="flex-row">
          <Typography type="title" color="inherit" className="fill flex-row flex-center-align" style={{ display: 'flex' }}>
            <Identicon hash={store.account.info.team} size={22} className="avatar" />
            {store.account.info.team}
          </Typography>
          {roles.indexOf('admin') > -1 &&
            <IconButton color="contrast" onClick={this.showAdmin}>
              <LockIcon />
            </IconButton>}
          <IconButton color="contrast" onClick={this.clearTeam}>
            <PoopleIcon />
          </IconButton>
          <IconButton color="contrast" onClick={this.logout}>
            <PowerIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
    )
  }
}
