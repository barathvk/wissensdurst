import { observer, inject } from 'mobx-react'
import UpIcon from 'material-ui-icons/KeyboardArrowUp'
import DownIcon from 'material-ui-icons/KeyboardArrowDown'
import RightIcon from 'material-ui-icons/KeyboardArrowRight'
import LeftIcon from 'material-ui-icons/KeyboardArrowLeft'
import IconButton from 'material-ui/IconButton'
import RefreshIcon from 'material-ui-icons/Refresh'
@inject('store')
@observer
export default class Controls extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
    this.navigate = e => {
      this.props.store.qmstore.navigate(e)
    }
  }
  render() {
    return (
      <div className="flex-row fill">
        <div className="fill flex-center flex-column">
          <div className="flex-column">
            <div className="flex-row flex-center">
              <IconButton color="primary" onClick={this.navigate.bind(this, 'up')} className="qm-control">
                <UpIcon className="qm-control" />
              </IconButton>
            </div>
            <div className="flex-row flex-center">
              <IconButton color="primary" onClick={this.navigate.bind(this, 'left')} className="qm-control">
                <LeftIcon className="qm-control" />
              </IconButton>
              <IconButton color="primary" onClick={this.navigate.bind(this, 'right')} className="qm-control">
                <RightIcon className="qm-control" />
              </IconButton>
            </div>
            <div className="flex-row flex-center">
              <IconButton color="primary" onClick={this.navigate.bind(this, 'down')} className="qm-control">
                <DownIcon className="qm-control" />
              </IconButton>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
