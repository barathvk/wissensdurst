import { observer, inject } from 'mobx-react'
import TeamChooser from './TeamChooser'
import User from './User'
import Admin from './Admin'
import QuizMaster from './QuizMaster'
import BuzzWatch from './BuzzWatch'
import SlideShow from './SlideShow'
@inject('store')
@observer
export default class Home extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  render() {
    const store = this.props.store
    return (
      <div className="fill flex-column home">
        {(!store.account.info.roles || _.keys(store.account.info.roles).indexOf('admin') === -1 || !store.showAdmin) &&
          !store.account.loading &&
          !store.qm &&
          <div className="fill flex-column">
            {store.account.info.team && <User />}
            {!store.account.info.team && <TeamChooser />}
          </div>}
        {store.account.info.roles && _.keys(store.account.info.roles).indexOf('admin') > -1 && store.showAdmin && <Admin />}
        {(!store.account.info.roles || _.keys(store.account.info.roles).indexOf('admin') === -1 || !store.showAdmin) && store.qm && !store.slides && <QuizMaster />}
        {(!store.account.info.roles || _.keys(store.account.info.roles).indexOf('admin') === -1 || !store.showAdmin) && store.qm && store.slides && <SlideShow />}
        {(!store.slides || !store.qmstore || store.qmstore.slide > 0) && <BuzzWatch />}
      </div>
    )
  }
}
