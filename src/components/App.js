import { inject, observer } from 'mobx-react'
import { CircularProgress } from 'material-ui/Progress'
import Login from './Login'
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles'
import createPalette from 'material-ui/styles/palette'
import levertonTheme from '../js/theme'
import Home from './Home'
@inject('store')
@observer
export default class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  render() {
    const theme = createMuiTheme({
      palette: createPalette({
        primary: levertonTheme,
        type: 'light'
      })
    })
    const store = this.props.store
    return (
      <MuiThemeProvider theme={theme}>
        <div className="fill flex-column">
          {store.loading &&
            <div className="fill flex-column flex-center login">
              <CircularProgress className="white-progress" />
            </div>}
          {!store.loading &&
            <div className="fill flex-column">
              {store.account.user && <Home />}
              {!store.account.user && <Login />}
            </div>}
        </div>
      </MuiThemeProvider>
    )
  }
}
