import { observer, inject } from 'mobx-react'
import Card, { CardActions, CardContent } from 'material-ui/Card'
import Button from 'material-ui/Button'
import TextField from 'material-ui/TextField'
import Typography from 'material-ui/Typography'
import AnnouncementIcon from 'material-ui-icons/Announcement'
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List'
import Identicon from 'react-identity-icon'
@inject('store')
@observer
export default class TeamChooser extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
    this.setTeam = e => {
      this.setState({ team: e.target.value })
    }
    this.createTeam = () => {
      this.props.store.account.setTeam(this.state.team)
    }
    this.chooseTeam = e => {
      this.props.store.account.setTeam(e)
    }
  }
  render() {
    const store = this.props.store
    return (
      <div className="fill flex-column padding-16 team-chooser">
        <Card style={{ marginBottom: '16px' }} className="fill flex-column padding-8">
          <Typography type="headline" style={{ color: '#AAA' }}>
            Choose a Team
          </Typography>
          {store.teams &&
            store.teams.length > 0 &&
            <List>
              {store.teams.map(t => {
                return (
                  <ListItem button key={t} onClick={this.chooseTeam.bind(this, t)}>
                    <ListItemIcon>
                      <Identicon hash={t} size={24} className="avatar bordered" />
                    </ListItemIcon>
                    <ListItemText primary={t} />
                  </ListItem>
                )
              })}
            </List>}
          {(!store.teams || store.teams.length === 0) &&
            <div className="fill flex-column flex-center ">
              <Typography type="headline" className="flex-column flex-center" style={{ display: 'flex', color: '#AAA' }}>
                <AnnouncementIcon style={{ width: 70, height: 70 }} />
                No Teams Available
              </Typography>
            </div>}
        </Card>
        <Card className="padding-8 flex-column">
          <Typography type="headline" style={{ color: '#AAA' }}>
            Create a Team
          </Typography>
          <CardContent style={{ padding: '8px' }} className="flex-column">
            <TextField label="Team Name" value={this.state.team || ''} onChange={this.setTeam} />
          </CardContent>
          <CardActions className="flex-row flex-center-align">
            <Button dense raised color="primary" className="fill" onClick={this.createTeam}>
              Create
            </Button>
            <Button dense raised color="accent" className="fill" onClick={store.account.logout.bind(this)}>
              Cancel
            </Button>
          </CardActions>
        </Card>
      </div>
    )
  }
}
