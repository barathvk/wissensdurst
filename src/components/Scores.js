import { observer, inject } from 'mobx-react'
import Table, { TableBody, TableCell, TableHead, TableRow } from 'material-ui/Table'
import Identicon from 'react-identity-icon'
@inject('store')
@observer
export default class Scores extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  render() {
    const store = this.props.store
    const grouped = _.groupBy(store.scores, s => s.team)
    let scores = _.keys(grouped).map(g => ({ team: g, points: _.sum(grouped[g].map(p => p.points)) }))
    scores = _.reverse(_.sortBy(scores, s => s.points))
    return (
      <div className="fill flex-column animated slideInRight">
        <Table>
          <TableHead>
            <TableRow>
              <TableCell />
              <TableCell>Team</TableCell>
              <TableCell>Points</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {scores.map(s => {
              return (
                <TableRow key={s.team} className={s.team === store.account.info.team ? 'yellow-back' : ''}>
                  <TableCell>
                    <Identicon hash={s.team} size={24} className="avatar bordered" />
                  </TableCell>
                  <TableCell>
                    {s.team}
                  </TableCell>
                  <TableCell>
                    {s.points}
                  </TableCell>
                </TableRow>
              )
            })}
          </TableBody>
        </Table>
      </div>
    )
  }
}
