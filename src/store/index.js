import { observable, action } from 'mobx'
import firebase from 'firebase'
import Account from './account'
import QMStore from './qm'
import moment from 'moment'
firebase.initializeApp(CONFIG.firebase)
export default class {
  @observable loading = true
  @observable account = new Account(this)
  @observable qmstore
  @observable teams
  @observable quiz
  @observable qm
  @observable scores
  @observable buzzed = []
  @observable showAdmin = false
  @observable slides = false
  @action
  async loadTeams() {
    const users = await firebase.database().ref('users').once('value').then(s => s.val())
    this.teams = _.uniq(_.values(users).map(u => u.team))
  }
  @action
  async load() {
    this.loading = true
    await this.account.load()
  }
  @action
  async loadQuiz() {
    const quizzes = await firebase.database().ref('quizzes').once('value').then(s => s.val())
    const quiz = _.first(_.keys(quizzes), k => quizzes[k].current)
    const q = quizzes[quiz]
    q.id = quiz
    this.quiz = q
    this.qm = this.quiz.qm === this.account.user.uid
    if (this.qm) {
      this.qmstore = new QMStore(this)
      await this.qmstore.load()
    }
  }
  @action
  async buzz() {
    if (window.navigator.vibrate) {
      window.navigator.vibrate(300)
    }
    const buzzed = (await firebase.database().ref(`quizzes/${this.quiz.id}/buzzed`).once('value').then(s => s.val())) || {}
    if (_.keys(buzzed).indexOf(this.account.info.team) === -1) {
      await firebase.database().ref(`quizzes/${this.quiz.id}/buzzed/${this.account.info.team}`).set({ suer: this.account.user.email, timestamp: moment().valueOf() })
    }
  }
  @action
  async reset() {
    await firebase.database().ref(`quizzes/${this.quiz.id}/buzzed`).set({})
  }
}
