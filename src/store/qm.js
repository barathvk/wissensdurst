import { observable, action } from 'mobx'
import firebase from 'firebase'
export default class {
  constructor(parent) {
    this.parent = parent
  }
  @observable slide = 0
  @observable question
  @observable content = require('../js/slides.html')
  @action
  async load() {
    firebase.database().ref(`quizzes/${this.parent.quiz.id}/slide`).on('value', async sn => {
      this.slide = sn.val() || 0
    })
  }
  @action
  makeid() {
    let text = ''
    const possible = 'abcdefghijklmnopqrstuvwxyz'
    for (let i = 0; i < 7; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length))
    }
    return text
  }
  @action
  async plus(buzz) {
    const tid = this.makeid()
    const quizid = this.parent.quiz.id
    await firebase.database().ref(`scores/${quizid}/${tid}`).set({ ...buzz, points: 1 })
  }
  @action
  async plushalf(buzz) {
    const tid = this.makeid()
    const quizid = this.parent.quiz.id
    await firebase.database().ref(`scores/${quizid}/${tid}`).set({ ...buzz, points: 0.5 })
  }
  @action
  async minus(buzz) {
    const tid = this.makeid()
    const quizid = this.parent.quiz.id
    await firebase.database().ref(`scores/${quizid}/${tid}`).set({ ...buzz, points: -1 })
  }
  @action
  async next(team) {
    const quizid = this.parent.quiz.id
    await firebase.database().ref(`quizzes/${quizid}/buzzed/${team}`).remove()
  }
  @action
  async navigate(e) {
    window.navigator.vibrate(300)
    const quizid = this.parent.quiz.id
    if (e === 'right') {
      this.parent.reset()
      await firebase.database().ref(`quizzes/${quizid}/slide`).set(this.slide + 1)
    } else if (e === 'left') {
      let slnum = this.slide - 1
      if (slnum < 0) {
        slnum = 0
      }
      await firebase.database().ref(`quizzes/${quizid}/slide`).set(slnum)
    } else if (e === 'reset') {
      await firebase.database().ref(`quizzes/${quizid}/slide`).set(0)
    }
  }
}
