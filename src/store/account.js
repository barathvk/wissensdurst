import { observable, action } from 'mobx'
import firebase from 'firebase'
export default class {
  @observable user
  @observable info = {}
  @observable loading = true
  @observable error
  constructor(parent) {
    this.parent = parent
  }
  @action
  async load() {
    firebase.auth().onAuthStateChanged(async user => {
      this.user = user
      if (this.user) {
        this.info = await firebase.database().ref(`users/${this.user.uid}`).once('value').then(u => (u.val() ? u.val() : {}))
        firebase.database().ref('users').on('value', () => {
          this.parent.loadTeams()
        })
        await this.parent.loadQuiz()
        firebase.database().ref(`scores/${this.parent.quiz.id}`).on('value', sn => {
          this.parent.scores = _.values(sn.val())
        })
        firebase.database().ref(`quizzes/${this.parent.quiz.id}/buzzed`).on('value', sn => {
          this.parent.buzzed = sn.val() || []
        })
      }
      this.loading = false
      this.parent.loading = false
    })
  }
  @action
  async setRole(role) {
    await firebase.database().ref(`users/${this.user.uid}`).set({ ...this.info, role })
    this.info = await firebase.database().ref(`users/${this.user.uid}`).once('value').then(u => (u.val() ? u.val() : {}))
  }
  @action
  async setTeam(team) {
    if (team) {
      this.info.team = team
    } else {
      delete this.info.team
    }
    await firebase.database().ref(`users/${this.user.uid}`).set(this.info)
    this.info = await firebase.database().ref(`users/${this.user.uid}`).once('value').then(u => (u.val() ? u.val() : {}))
  }
  @action
  async signup(email, password) {
    this.loading = true
    this.error = null
    await firebase.auth().createUserWithEmailAndPassword(email.trim(), password).then(() => this.login(email.trim(), password)).catch(err => {
      this.loading = false
      this.error = err
    })
  }
  @action
  async login(email, password) {
    this.loading = true
    this.error = null
    firebase.auth().signInWithEmailAndPassword(email.trim(), password).catch(err => {
      this.loading = false
      this.error = err
    })
  }
  @action
  async logout() {
    await firebase.auth().signOut()
  }
}
