export default {
  50: '#e3f0f3',
  100: '#b8dae2',
  200: '#89c2cf',
  300: '#5aa9bb',
  400: '#3696ad',
  500: '#13849e',
  600: '#117c96',
  700: '#0e718c',
  800: '#0b6782',
  900: '#065470',
  A100: '#a0e2ff',
  A200: '#6dd3ff',
  A400: '#3ac3ff',
  A700: '#20bcff',
  contrastDefaultColor: 'light'
}
